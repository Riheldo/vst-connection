import { VsConnectionAbs, VsRequestMethod, UseConnectionImpl } from '../lib/public_api';
import { VsEndpoint } from '../lib/models/vst-endpoint.model';
import { STRRegistrarInteressado, STRToken, STRBancoDAO, STRStatusSD } from './vicax_stress_openapi_oo';
var rp = require('request-promise-native');


class VstConnection extends VsConnectionAbs<any, any> {
    HOST = "http://192.168.1.150:8383";
    protected makeRequest(endpoint: VsEndpoint, method: VsRequestMethod, body?: any, headers?: any): Promise<any> {
        console.log("\n------------------------------------------- REQUEST PARAMS -------------------------------------------");
        console.log("URI:    ", endpoint.toUri());
        console.log("Metodo:      ", method);
        console.log("Body:        ", body);
        console.log("Headers:     ", headers);
        console.log("----------------------------------------- END REQUEST PARAMS -----------------------------------------\n");
        return rp({json: true, uri: endpoint.toUri(), method: method, headers: headers, body: body});
    }

    btoa(str) {
        if (Buffer.byteLength(str) !== str.length) throw new Error('bad string!');
        return (new Buffer(str, 'binary')).toString('base64');
    }
}

const conn = new VstConnection()
conn.clientID = "ba055efa-f1ae-46b7-b4df-92f64f460467";
conn.clientSecret = "A3tL2sX4sT3bU6wY7iD7kB3lJ0jN7nQ0mV8yO2oH2lH2dG1mT1";
UseConnectionImpl(conn);



// const reg = new STRRegistrarInteressado();
// reg.input.email = "Riheldomelo@hotmail.com"
// reg.execute().then((res) => {
//     console.log("STRRegistrarInteressado output", res);
// }).catch((rea) => {
//     console.log("STRRegistrarInteressado exception", rea);
// });


const inToken = new STRToken();
inToken.execute().then((res: any) => {
    console.log("sucesso para login!");
    
    const banco = new STRBancoDAO();
    banco.input.codigo = 70;
    banco.list(null, {take: 3, skip:0}).then((res) => {
        console.log("List banco", res);
    }).catch((er) => {
        console.log("Banco error", er)
    });

    // const insertBanco = new STRBancoDAO();
    // insertBanco.input.codigo = 11102;
    // insertBanco.input.nome = "Banco Riheldo asjkldkljsadkljksaljçksadljsadlçsadj";
    // insertBanco.insert().then((res) => {
    //     console.log("Banco inserido com código: ", res.codigo);
    // }).catch((er) => {
    //     console.log("Não foi possível inserir Banco. Erro: ", er.message)
    // });



    // STRStatusSD.populate((data) => {
    //     console.log("Populate", data);
    // })

}).catch((rej) => console.log("onn rejeitada", rej));
