/* tslint:disabled */
// NOT DELETE !!!!!!!!!!!!!
export const TDomains: any = {};
// NOT DELETE !!!!!!!!!!!!!



TDomains.STRAuthorType_OFFICIAL = 'Autor Oficial';
TDomains.STRAuthorType_CO_AUTHOR = 'Co-autor';
TDomains.STRAuthorType_SUPPORT = 'Suporte';
TDomains.STRProjectStatus_NEW = 'Novo';
TDomains.STRProjectStatus_EDIT = 'Editavel';
TDomains.STRProjectStatus_PUBLISHED = 'Publicado';
TDomains.STRProjectStatus_BLOCKED = 'Bloqueado';
TDomains.STRProjectStatus_CANCELED = 'Cancelado';
TDomains.STRProjectStatus_SUSPENDED = 'Suspenso';
TDomains.STRProjectStatus_CLOSED = 'Encerrado';
TDomains.STRStatus_ACTIVE = 'Ativo';
TDomains.STRStatus_BLOCKED = 'Bloqueado';
TDomains.STRStatus_CANCELED = 'Cancelado';
TDomains.STRStatus_SUSPENDED = 'Suspenso';
TDomains.STRStatus_CLOSED = 'Encerrado';
TDomains.STRSponsorType_OFFICIAL = 'Patrocinador Oficial';
TDomains.STRSponsorType_CO_SPONSOR = 'Co-patrocinador';
TDomains.STRSponsorType_MEDIA_SPONSOR = 'Patrocinador mídia';
TDomains.STRSponsorType_SUPPORT = 'Suporte';
