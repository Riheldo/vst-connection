import { VsCrudObject } from '../models/vst-object-crud.model';
import { Type } from '../interfaces/vst-type.interface';
import { VsConnectionAbs } from '../models/vst-connection.model';

let __USE_INSTANCE_CONNECTION: VsConnectionAbs<any, any> = null;

export function UseConnectionImpl (connInstance: VsConnectionAbs<any, any>) {
    __USE_INSTANCE_CONNECTION = connInstance;
}

export function UsedConnectionImpl(): VsConnectionAbs<any, any> {
    return __USE_INSTANCE_CONNECTION;
}

export function UseClass(uc: {i?: Type<any>, o?: Type<any>}) {
    return (target: Type<VsCrudObject<any, any>>) => {
        (target as any).useClass(uc);
        (target as any).thisClass = target;
    }
}