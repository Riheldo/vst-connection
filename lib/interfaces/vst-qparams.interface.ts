export interface VsQueryParamsI {
    [key: string]: string | number | boolean;
}

export interface VsPathParamsI {
    [key: string]: string;
}