export interface VsDocObject {
    onInitialized?: () => void;
    onSettingData?: (data: any) => void;
    onDataSet?: () => void;
    toJson?: () => object;
}