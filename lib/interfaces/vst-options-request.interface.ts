export interface VsOptionsReq {
    timeout?: number;
    redirect?: string;
    language?: string;
    format?: boolean;
    country?: string;
    qparams?: {[key: string]: string | number | boolean};
    authentication?: boolean;
    ticket?: string;
}
export interface VsGetOptionsReq extends VsOptionsReq {
    skip?: number;
    take?: number;
    filter?: string;
    requireTotalCount?: true;
    sort?: string;
}
export interface VsInsertOptionsReq extends VsOptionsReq {}
export interface VsUpdateOptionsReq extends VsOptionsReq {}
export interface VsDeleteOptionsReq extends VsOptionsReq {}