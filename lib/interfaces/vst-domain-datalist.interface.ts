export interface VsDomainDataListInput {
    domain?: string; key?: string; value?: string; filter?: string;
    process?: string; vendor?: string; module?: string; version?: string;
}

export interface VsDomainDataListOutput {
    domain: string;
    data: string;
}