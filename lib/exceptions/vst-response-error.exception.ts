export class VsResponseException extends Error {
    
    name: string;    
    stack: string;
    message: string;
    statusCode: number;
    returnCode: number;

    constructor(message) {
        super(message);
    }
}