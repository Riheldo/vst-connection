export enum VsProtocol {
    HTTP       = 'http',
    HTTPS      = 'https',
}