export enum VsRequestOperation {
    GET = 'GET',        // GET
    POST = 'POST',      // POST
    PUT = 'PUT',        // PUT
    DELETE = 'DELETE',  // DELETE
    PATCH = 'PATCH',    // PATCH
    CRUD = 'CRUD',      // CRUD
    DD = 'DD',          // DD
    LIST = 'LIST',
}