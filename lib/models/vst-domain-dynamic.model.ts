import { VsDomainDataListInput } from "../interfaces/vst-domain-datalist.interface";
import { VsDomainInterface } from "../interfaces/vst-domain.interface";
import { VsDomainDataList } from "./vst-domain-datalist.model";

/**
 * Realiza a leitura de um dominio no vset e retorna uma lista de chave valor,
 * funcionalidade para ser usada em cruds
 */
export abstract class VsDynamicDomain {
    protected static className: string = '';
    protected static dataListConfig: VsDomainDataListInput;
    private static cache: Array<{ key: string, domains: Array<VsDomainInterface> }>;
    private static lastUpdates: Array<{ key: string, dt: Date }>;
    private static loadings: Array<{ key: string, prm: Promise<Array<VsDomainInterface>> }>;
    private static subscriptions: Array<{ key: string, population: Array<VsDomainInterface> }> = [];
    static cacheTimeout: number = 1500; // ms

    /**
     * Retorna uma promessa de dominios, e/ou popula uma lista que eh passada como parametro
     * @param key valor do campo que chave que eh usado como foregein key
     * @param value valor do campo da tabela que representa a chave
     * @param force faz a requisicao ao vset, nao faz leitura do cache
     * @param list list a ser preenchida quando obtiver resultado
     * @param map (opcional) funcao de estrutura (key: number|string, value: string, index: number) => VsDomainInterface, trata o objeto a ser incluido na lista
     */
    static populate(key: string, value: string, filter?: string, force: boolean = false, list?: Array<object>, map?: (key: number | string, value: string, index: number) => VsDomainInterface): Promise<Array<VsDomainInterface>> {
        const prms: Promise<Array<VsDomainInterface>> = this.load(key, value, filter, force);
        prms.then((domains: Array<VsDomainInterface>) => {
            if (list) {
                domains.forEach((domain: VsDomainInterface, index: number) => {
                    if (map) list.push(map(domain.key, domain.value, index));
                    else list.push({ index: index, key: domain.key, value: domain.value } as VsDomainInterface);
                });
            }
        });
        return prms;
    }
    static subscribePopulation(list: Array<VsDomainInterface>, key: string, value: string): () => void {
        if (!this.subscriptions) this.subscriptions = [];
        const i: number = this.subscriptions.length;
        this.subscriptions.push({ key: key + value, population: list });
        return () => {
            this.subscriptions.splice(i, 1);
        };
    }
    private static updatePopulations(key: string): void {
        if (!this.subscriptions) this.subscriptions = [];
        for (let i: number = 0; i < this.subscriptions.length; i++) {
            if (this.subscriptions[i].key === key) {
                const auxPopulation: Array<VsDomainInterface> = this.subscriptions[i].population;
                auxPopulation.splice(0, auxPopulation.length);
                auxPopulation.push(...this.getCacheDomains(key));
            }
        }
    }
    private static getCacheDomains(key: string): Array<VsDomainInterface> {
        if (!this.cache) this.cache = [];
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].key === key) {
                return this.cache[i].domains;
            }
        }
    }
    private static getLastUpdate(key: string): Date {
        if (!this.lastUpdates) this.lastUpdates = [];
        for (let i = 0; i < this.lastUpdates.length; i++) {
            if (this.lastUpdates[i].key === key) return this.lastUpdates[i].dt;
        }
    }
    private static setLastUpdate(key: string, date: Date): void {
        const cachedDate: Date = this.getLastUpdate(key);
        for (let i = 0; i < this.lastUpdates.length; i++) {
            if (this.lastUpdates[i].key === key) {
                this.lastUpdates[i].dt = date;
                return;
            }
        }
        this.lastUpdates.push({ key: key, dt: date });
    }
    private static updateCacheDomains(key: string, domains: Array<VsDomainInterface>): void {
        if (!this.cache) this.cache = [];
        for (let i = 0; i < this.cache.length; i++) {
            if (this.cache[i].key === key) {
                const domainsInCache: Array<VsDomainInterface> = this.cache[i].domains;
                domainsInCache.splice(0, domainsInCache.length);
                domainsInCache.push(...domains);
                this.setLastUpdate(key, new Date());
                this.updatePopulations(key);
                return;
            }
        }
        this.cache.push({ key: key, domains: domains });
        this.setLastUpdate(key, new Date());
        this.updatePopulations(key);
    }
    private static getOnLoading(key): Promise<Array<VsDomainInterface>> {
        if (!this.loadings) this.loadings = [];
        for (let i = 0; i < this.loadings.length; i++) {
            if (this.loadings[i].key === key) return this.loadings[i].prm;
        }
    }
    private static setOnLoading(key: string, prm: Promise<Array<VsDomainInterface>>): void {
        const cachedLoading = this.getOnLoading(key);
        for (let i = 0; i < this.loadings.length; i++) {
            if (this.loadings[i].key === key) {
                this.loadings[i].prm = prm;
                return;
            }
        }
        this.loadings.push({ key: key, prm: prm });
    }
    private static load(key: string, value: string, filter: string, force: boolean = false): Promise<Array<VsDomainInterface>> {
        let onLoading: Promise<Array<VsDomainInterface>> = this.getOnLoading(key + value);
        if (onLoading) return onLoading;
        if (!force && this.getLastUpdate(key + value)) {
            if (!this.cacheTimeout) this.cacheTimeout = 900;
            // checa 900 segundos (15 minutos)
            const now: Date = new Date();
            const diff: number = now.getTime() - this.getLastUpdate(key + value).getTime();
            if (diff / 1000 < this.cacheTimeout) return new Promise((resolve: any, reject: any) => {
                resolve(this.getCacheDomains(key + value));
            });
        }
        onLoading = new Promise((resolve: (domains: Array<VsDomainInterface>) => void, reject: (reason: any) => void) => {
            const domainDataList: VsDomainDataList = new VsDomainDataList();

            const auxInputDL: VsDomainDataListInput = this.dataListConfig;
            auxInputDL.key = key;
            auxInputDL.value = value;
            auxInputDL.filter = filter;
            domainDataList.input.push(auxInputDL);
            domainDataList.execute().then((res) => {
                const domains: Array<VsDomainInterface> = JSON.parse(domainDataList.output[0].data);
                this.updateCacheDomains(key + value, domains);
                resolve(domains);
            }).catch((er) => {
                reject(er);
            }).finally(() => {
                this.setOnLoading(key + value, undefined);
            });
        });
        this.setOnLoading(key + value, onLoading);
        return onLoading;
    }
}