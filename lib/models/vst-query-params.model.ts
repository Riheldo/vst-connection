import { VsQueryParamsI } from '../interfaces/vst-qparams.interface';

export class VsQueryParams {
    private _value: VsQueryParamsI = {}

    get value() {
        return JSON.parse(JSON.stringify(this._value));
    }

    resolve(): string {
        return Object.keys(this._value).map((qkey) => qkey + "=" + this._value[qkey]).join("&");
    }

    push(key: string, val: string | number | boolean) {
        if (typeof val === "boolean") val = val ? 'true' : 'false';
        if (typeof val === "function" || typeof val === "object") throw new Error("Valor de entrada para qparams invalido!");
        this._value[key] = val;
    }

    pushMult(qps: VsQueryParams) {
        if (!qps) return;
        Object.keys(qps).forEach((qkey) => {
            this.push(qkey, qps[qkey]);
        })
    }
}