import { VsCrudObject } from './vst-object-crud.model';
import { VsFilterType } from '../interfaces/vst-filter-notations.interface';
import { VsOptionsReq } from '../interfaces/vst-options-request.interface';
import { VsRequestOperation } from '../enum/vst-request-operation.enum';

export abstract class VsBusinessObject<TI, TO> extends VsCrudObject<TI, TO> {
    protected objectOperation: VsRequestOperation; // para business object
    readonly keys: string[];
    
    execute(filter?: VsFilterType, options?: VsOptionsReq): Promise<TO | Array<TO>> {
        if (this.objectOperation === VsRequestOperation.DD) throw new Error("DD on VstBusinessConnection");
        if (this.objectOperation === VsRequestOperation.CRUD) throw new Error("CRUD on VstBusinessConnection");
        if (this.objectOperation === VsRequestOperation.GET) {
            // listar ou pegar
            return this.list(filter, options);
        }
        if (this.objectOperation === VsRequestOperation.POST) {
            // insert
            return this.insert(options);
        }
        if (this.objectOperation === VsRequestOperation.PUT || this.objectOperation === VsRequestOperation.PATCH) {
            // update
            return this.update(filter, options);
        }
        if (this.objectOperation === VsRequestOperation.DELETE) {
            // delete
            return this.delete(filter, options);
        }
        throw new Error("Reach in neverland!!");
    }
}