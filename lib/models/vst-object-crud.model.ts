import { VsDocObject } from '../interfaces/vst-doc-object.interface';
import { Type } from '../interfaces/vst-type.interface';
import { VsConnectionAbs } from './vst-connection.model';
import { VsInsertOptionsReq, VsUpdateOptionsReq, VsGetOptionsReq, VsDeleteOptionsReq, VsOptionsReq } from '../interfaces/vst-options-request.interface';
import { VsFilterType } from '../interfaces/vst-filter-notations.interface';
import { VsRequestOperation } from '../enum/vst-request-operation.enum';
import { VsEndpoint } from './vst-endpoint.model';
import { UsedConnectionImpl } from '../decoratorss/vst-useclass.decorator';

export abstract class VsCrudObject<TI, TO> {

    // vst options
    public timeout: number;
    public language: string;
    public format: boolean;
    public country: string;
    public params: {[key: string]: string | number | boolean} = {};
    private _endpointModel: VsEndpoint;
    public get endpointModel(): VsEndpoint {
        if (!this._endpointModel) this._endpointModel = new VsEndpoint(this.endpoint);
        return this._endpointModel;
    }

    // execute config
    abstract readonly endpoint: string; // endpoint
    readonly ticket: string; // publico
    readonly redirect: string; // opcional
    abstract readonly keys: Array<string> = []; // chaves primarias
    readonly inputFields: Array<string> = [];
    readonly authentication: boolean = false;
    
    private readonly useClassInput: Type<TI>;
    useClassOutput: Type<TO>;
    static useClassInput: Type<any>;
    static useClassOutput: Type<any>;
    
    readonly input: TI;
    output: TO | Array<TO> = [];

    static thisClass: Type<VsCrudObject<any, any>>;

    constructor() {
        if (this.useClassInput) this.input = new this.useClassInput();
    }

    private conn(): VsConnectionAbs<TI, TO> {
        return UsedConnectionImpl();
    }
    
    fillOutput(data: any) {
        if (data instanceof Array) {
            this.output = data.map((itemData) => {
                const instanceItem = new this.useClassOutput();
                const auxImpInstance: VsDocObject = instanceItem;
                if (typeof auxImpInstance.onInitialized === "function") auxImpInstance.onInitialized();
                if (typeof auxImpInstance.onSettingData === "function") auxImpInstance.onSettingData(itemData);
                else Object.assign(instanceItem, itemData);
                if (typeof auxImpInstance.onDataSet === "function") auxImpInstance.onDataSet();
                return instanceItem;
            });
        } else if (typeof data === "object") {
            const instanceItem = new this.useClassOutput();
            const auxImpInstance: VsDocObject = instanceItem;
            if (typeof auxImpInstance.onInitialized === "function") auxImpInstance.onInitialized();
            if (typeof auxImpInstance.onSettingData === "function") auxImpInstance.onSettingData(data);
            else Object.assign(instanceItem, data);
            if (typeof auxImpInstance.onDataSet === "function") auxImpInstance.onDataSet();
            this.output = instanceItem;
        } else console.error("Tipo de output data invalido")
        
    }

    inputToJson() {
        const auxImpInstance: VsDocObject = this.input;
        if (typeof auxImpInstance.toJson === "function") {
            return auxImpInstance.toJson();
        } else {
            const inputObject: any = {};
            Object.keys(this.input).forEach((field) => {
                inputObject[field] = this.input[field];
            });
            return inputObject;
        }
    }

    private treatComeBack(p: Promise<any>): Promise<any> {
        p.then((res) => {
            if (this.useClassOutput) {
                if (res) this.fillOutput(res);
            } else {
                this.output = res;
            }
            return this.output;
        });
        return p;
    }

    getOptions (options: VsOptionsReq): VsOptionsReq {
        if (!options) options = {};
        
        if (this.authentication != undefined) options.authentication = this.authentication;
        if (this.ticket != undefined) options.ticket = this.ticket;
        if (this.redirect != undefined) options.redirect = this.redirect;
        
        return options;
    }

    fillPpEndpoint(minPP: number, maxPP: number) {
        if (!this.endpointModel.requestKeys.length || maxPP === 0) return;
        let countSub = 0;
        this.endpointModel.requestKeys.forEach((pkey) => {
            for (const field in this.input) {
                if (this.input.hasOwnProperty(field)) {
                    if (pkey === "{"+field+"}") {
                        const valueField: any = this.input[field];
                        if (valueField != undefined) {
                            this.endpointModel.addPparam(field, valueField);
                            countSub++;
                        }
                    }
                }
                if (countSub == maxPP) break;
            }
        });
        if (countSub < minPP) throw new Error("Falta paths parametros!");
    }

    select(filter?: VsFilterType, options?: VsGetOptionsReq): Promise<TO> {
        options = this.getOptions(options);
        this.fillPpEndpoint(this.endpointModel.requestKeys.length, this.endpointModel.requestKeys.length);
        return this.treatComeBack(this.conn().get(this.endpointModel, filter, options));
    }

    list(filter?: VsFilterType, options?: VsGetOptionsReq): Promise<Array<TO>> {
        options = this.getOptions(options);
        this.fillPpEndpoint(this.endpointModel.requestKeys.length-1, this.endpointModel.requestKeys.length-1);
        return this.treatComeBack(this.conn().list(this.endpointModel, filter, options));
    }

    insert(options?: VsInsertOptionsReq): Promise<TO> {
        options = this.getOptions(options);
        this.fillPpEndpoint(this.endpointModel.requestKeys.length-1, this.endpointModel.requestKeys.length-1);
        const data = this.inputToJson();
        return this.treatComeBack(this.conn().insert(this.endpointModel, data, options));
    }
    
    update(filter?: VsFilterType, options?: VsUpdateOptionsReq) {
        options = this.getOptions(options);
        this.fillPpEndpoint(this.endpointModel.requestKeys.length, this.endpointModel.requestKeys.length);
        const data = this.inputToJson();
        if (this.keys) this.keys.forEach((keyField) => {
            delete data[keyField];
        });
        return this.treatComeBack(this.conn().update(this.endpointModel, data, filter, options));
    }

    delete(filter?: VsFilterType, options?: VsDeleteOptionsReq) {
        options = this.getOptions(options);
        this.fillPpEndpoint(this.endpointModel.requestKeys.length, this.endpointModel.requestKeys.length);
        return this.conn().delete(this.endpointModel, filter, options);
    }

    static useClass(io: {i?: Type<any>, o?: Type<any>}) {
        if (io.i) this.useClassInput = io.i as any;
        if (io.o) this.useClassOutput = io.o as any;
        (this.prototype as any).useClassInput = this.useClassInput;
        (this.prototype as any).useClassOutput = this.useClassOutput;
    }

    static setTimeout(timeout?: number): void {
        this.prototype.timeout = timeout;
    }
    static setLanguage(language?: string): void {
        this.prototype.language = language;
    }
    static setFormat(format?: boolean): void {
        this.prototype.format = format;
    }
    static setCountry(country?: string): void {
        this.prototype.country = country;
    }

    static fabricate() {
        return new this.thisClass();
    }
}
