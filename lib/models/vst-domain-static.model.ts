import { VsDomainInterface } from "../interfaces/vst-domain.interface";
import { Subject, Subscription, VirtualTimeScheduler, Observable, onErrorResumeNext } from 'rxjs';
/**
 * Realiza a leitura de um enumeration e retorna uma lista de chave valor,
 * funcionalidade para ser usada em cruds
 */
export abstract class VsStaticDomain {
    protected static readonly DomainName: string;
    protected static readonly DomainEnum: any;
    protected static readonly TFileDomains: any;

    private static subscriptions: Subject<Array<VsDomainInterface>> = new Subject();
    // private static subscriptions: Observable<Array<VsDomainInterface>> = new Observable();
    
    private static getContent(): Array<VsDomainInterface> {
        const auxList: Array<VsDomainInterface> = [];
        Object.keys(this.DomainEnum).map((val: string, index: number) => {
            const key: string = this.DomainEnum[val];
            const desc: string = this.TFileDomains[this.DomainName + '_' + val];
            auxList.push({ key: key, value: desc, index: index });
        });
        return auxList;
    }

    static populate(cb: (data: Array<VsDomainInterface>) => void): Subscription {
        setTimeout(() => {
            try {
                cb(this.getContent());
            } catch (error) {
                console.error(error);
            }
        });
        const subs = this.subscriptions.subscribe((val) => {
            try {
                return cb(val);
            } catch (error) {
                console.error(error);
            }
        });
        return subs;
    }


    static fire() {
        this.subscriptions.next(this.getContent());
    }
}