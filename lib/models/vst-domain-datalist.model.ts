import { VsBusinessObject } from "./vst-object-business.model";
import { VsRequestOperation } from "../enum/vst-request-operation.enum";
import { VsDomainDataListOutput, VsDomainDataListInput } from "../interfaces/vst-domain-datalist.interface";

/**
 * VSDomainDataList tem funcionamento interno, nao eh exportado desse modulo.
 * Realiza a interface de execucao do processo DomainDataList que captura informacaoes
 * de uma tabela especifica para ser usado em recursos em cruds
 */
export class VsDomainDataList extends VsBusinessObject<any, any> {
    input: Array<VsDomainDataListInput> = [];
    output: Array<VsDomainDataListOutput>;
    private vendor: 'VICAX';
    private module: 'TOOLS';
    private version: '1.0.0'; 
    requestMethod: VsRequestOperation.DD;
    endpoint: '/app/tools/DomainDataList';
}