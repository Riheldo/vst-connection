import { VsRequestMethod } from '../enum/vst-request-method.enum';
import { VsFilterType } from '../interfaces/vst-filter-notations.interface';
import { VsGetOptionsReq, VsInsertOptionsReq, VsUpdateOptionsReq, VsDeleteOptionsReq, VsOptionsReq } from '../interfaces/vst-options-request.interface';
import { VsEndpoint } from './vst-endpoint.model';
import { VsResponseException } from '../exceptions/vst-response-error.exception';

export abstract class VsConnectionAbs<T, S> {

    // HOST
    public HOST:string = "";
    // VST OPTIONS
    public timeout: number;
    public language: string;
    public format: boolean;
    public country: string;

    tokenBearer: string;
    clientID: string;
    clientSecret: string;

    protected abstract makeRequest(endpoint: VsEndpoint, method: VsRequestMethod, body?: T, headers?: any): Promise<any>;

    btoa(str: string): string {
        return btoa(str);
    }

    protected callbackResponse(res: any): any {
    }

    private _makeRequest(endpoint: VsEndpoint, method: VsRequestMethod, options: VsOptionsReq, body?: T, headers?: any): Promise<any> {
        //VERIFICA SE DEVE ATRIBUIR O CABEÇALHO BASIC OU BEARER
        if (!headers) headers = {};
        if ((options.ticket || options.authentication))
            Object.assign(headers, { 'authorization': 'Basic ' + this.btoa(this.clientID + ':' + this.clientSecret) });
        else if (this.tokenBearer)
            Object.assign(headers, { 'authorization': ('Bearer ' + this.tokenBearer) });

        Object.assign(headers, {"Content-Type": "application/json; charset=utf-8"});

        endpoint.host = this.HOST;
        return new Promise<any>((resolve, reject) => {
            this.makeRequest(endpoint, method, body, headers).then((res) => {
                const result = this.callbackResponse(res);
    
                if (options.authentication) {
                    // this.conn.userIsLoggedIn = true;
                    // SALVA O TOKEN DE AUTENTICAÇÃO
                    let token = (res as any).access_token;
                    this.tokenBearer = token;
                }
    
                if (result !== undefined) return resolve(result);
                return resolve(res);
            }).catch((reason) => {
                if (reason && reason.error && reason.error.returnCode === -8) resolve([]);
                if (reason.error && reason.error.message) {
                    const resError: VsResponseException = new VsResponseException(reason.error.message);
                    resError.name = reason.name;
                    resError.returnCode = reason.error.returnCode;
                    resError.stack = reason.stack;
                    resError.statusCode = reason.statusCode;
                    reject(resError);
                }
                reject(reason);
            });
        });
    }

    get(endpoint: VsEndpoint, filter?: VsFilterType, options?: VsGetOptionsReq): Promise<S> {
        this.resolveReadOptions(endpoint, options || {});
        this.resolveFilter(endpoint, filter);
        return this._makeRequest(endpoint, VsRequestMethod.GET, options);
    }

    list(endpoint: VsEndpoint, filter?: VsFilterType, options?: VsGetOptionsReq): Promise<Array<S>> {
        this.resolveReadOptions(endpoint, options || {});
        this.resolveFilter(endpoint, filter);
        return this._makeRequest(endpoint, VsRequestMethod.GET, options);
    }

    insert(endpoint: VsEndpoint, data: T, options?: VsInsertOptionsReq) {
        this.resolveCommomOptions(endpoint, options || {});
        return this._makeRequest(endpoint, VsRequestMethod.POST, options, data);
    }
    
    update(endpoint: VsEndpoint, data: T, filter: VsFilterType, options?: VsUpdateOptionsReq) {
        this.resolveCommomOptions(endpoint, options || {});
        this.resolveFilter(endpoint, filter);
        return this._makeRequest(endpoint, VsRequestMethod.PATCH, options, data);
    }

    delete(endpoint: VsEndpoint, filter: VsFilterType, options?: VsDeleteOptionsReq) {
        this.resolveCommomOptions(endpoint, options || {});
        this.resolveFilter(endpoint, filter);
        return this._makeRequest(endpoint, VsRequestMethod.DELETE, options);
    }

    protected resolveFilter(endpoint: VsEndpoint, filter: VsFilterType): void {
        if (typeof filter === "string") {
            if (filter) endpoint.addQParam("$filter", filter);
        } else {
            endpoint.addQParams(filter as any);
        }
    }

    protected resolveCommomOptions(endpoint: VsEndpoint, options: VsOptionsReq): void {
        if (!options) options = {};
        if (options.ticket) endpoint.addQParam("$ticket", options.ticket);
        if (options.country) endpoint.addQParam("$country", options.country);
        if (options.format) endpoint.addQParam("$format", options.format);
        if (options.language) endpoint.addQParam("$language", options.language);
        if (options.timeout) endpoint.addQParam("$timeout", options.timeout);
        if (options.redirect) endpoint.addQParam("$redirect", options.redirect);
        if (options.qparams) endpoint.addQParams(options.qparams as any);
    }

    protected resolveReadOptions(endpoint: VsEndpoint, options: VsGetOptionsReq): void {
        if (!options) options = {};
        this.resolveCommomOptions(endpoint, options);
        if (options.requireTotalCount) endpoint.addQParam("VQPRequireTotalCount", options.requireTotalCount);
        if (options.skip) endpoint.addQParam("VQPSkip", options.skip);
        if (options.sort) endpoint.addQParam("VQPSort", options.sort);
        if (options.take) endpoint.addQParam("VQPTake", options.take);
    }

    // with(executionOf: string | NAPMiddleware | Array<NAPMiddleware>, midPosition: NapMidExecLapse = NapMidExecLapse.always): VstConnectionService {
    //     const connInstance = this.getDefaultConnection();
    //     return connInstance.with(executionOf, midPosition);
    // }

    static setTimeout(timeout?: number): void {
        this.prototype.timeout = timeout;
    }
    static setLanguage(language?: string): void {
        this.prototype.language = language;
    }
    static setFormat(format?: boolean): void {
        this.prototype.format = format;
    }
    static setCountry(country?: string): void {
        this.prototype.country = country;
    }
}
