import { VsProtocol } from '../enum/vst-request-protocol.enum';
import { VsQueryParams } from './vst-query-params.model';
import { VsPathParamsI } from '../interfaces/vst-qparams.interface';

const re = new RegExp(/{\w+}/gm);

export class VsEndpoint {

    private readonly qparams: VsQueryParams = new VsQueryParams();
    private readonly pparams: VsPathParamsI = {};
    private readonly _endpoint: string
    
    public protocol: VsProtocol = VsProtocol.HTTP;
    public host: string = "";
    public readonly requestKeys: Readonly<Array<string>>;
    public get value(): string {
        return this.getValue();
    }

    constructor (_endpoint: string) {
        // separar endpoint
        if (_endpoint.indexOf("?") != -1) {
            let auxEndpoint = _endpoint.split("?");
            _endpoint = auxEndpoint[0];
            auxEndpoint[1].split("&").forEach((pares) => {
                let auxPares = pares.split("=");
                this.addQParam(auxPares[0], auxPares[1]);
            });
        }
        const auxKeys = Object.freeze<Array<string>>(_endpoint.match(re) || []);
        this.requestKeys = auxKeys;
        this._endpoint = _endpoint;
    }

    toString() {
        return this.value;
    }

    toUri() {
        return this.host + this.getValue();
    }

    private getValue(): string {
        let endpoint = this.resolvePparam();
        
        const qparam: string = this.qparams.resolve();
        if (qparam) endpoint += "?" + qparam;

        if (!this.requestKeys.length) this.requestKeys.forEach((pkey) => {
            endpoint = endpoint.replace(pkey, "");
        });

        return endpoint;
    }

    addQParam(key: string, value: string | number | boolean) {
        this.qparams.push(key, value);
    }

    addQParams(qps: VsQueryParams) {
        this.qparams.pushMult(qps);
    }


    resolvePparam(): string {
        let endpoint = this._endpoint;
        if (!this.requestKeys.length) return endpoint;
        this.requestKeys.forEach((pkey) => {
            const subsVal: string = this.pparams[pkey];
            if (subsVal) endpoint = endpoint.replace(pkey, subsVal);
            else {
                const auxSplited = endpoint.split(pkey);
                // premissa de os tokens do path sao unicos
                endpoint = auxSplited[0] + auxSplited[1].substr(1);
            }
        });
        if (endpoint[endpoint.length - 1] == "/") endpoint = endpoint.substr(0, endpoint.length - 1);
        return endpoint;
    }

    addPparam(key: string, val: string | number | boolean) {
        if (typeof val === "boolean") val = val ? 'true' : 'false';
        if (typeof val === "function" || typeof val === "object") throw new Error("Valor de entrada para qparams invalido!");
        for (const auxPparan of this.requestKeys) {
            if (auxPparan === "{"+key+"}") {
                this.pparams[auxPparan] = String(val);
                return;
            }
        }
        throw new Error("Campo '" + key + "' nao eh path parametro de endpoint " + this._endpoint);
    }
}